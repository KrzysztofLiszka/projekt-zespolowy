﻿namespace PROJEKT.ZESPOLOWY.BACKEND.DTOs
{
    public class NewTeacherDto
    {
        public string? AcademicTitle { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
    }
}
