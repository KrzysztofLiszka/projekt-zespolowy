﻿namespace PROJEKT.ZESPOLOWY.BACKEND.DTOs
{
    public class NewHomeworkDto
    {
        public DateTime AddedDate { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string Subject { get; set; }
        public string Progress { get; set; }
    }
}
