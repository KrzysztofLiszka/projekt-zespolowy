﻿namespace PROJEKT.ZESPOLOWY.BACKEND.DTOs
{
    public class NewLessonsPlanDto
    {
        public string StartHour { get; set; }
        public string EndHour { get; set; }
        public string Subject { get; set; }
        public string Day { get; set; }
    }
}
