namespace PROJEKT.ZESPOLOWY.BACKEND.DTOs
{
    public class NewNoteDto
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public string Subject_name { get; set; }
        public DateTime Date { get; set; }
    }
}
