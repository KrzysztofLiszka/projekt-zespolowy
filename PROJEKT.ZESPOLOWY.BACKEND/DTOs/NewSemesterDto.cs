﻿namespace PROJEKT.ZESPOLOWY.BACKEND.DTOs
{
    public class NewSemesterDto
    {
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
