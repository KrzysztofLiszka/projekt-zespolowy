﻿namespace PROJEKT.ZESPOLOWY.BACKEND.DTOs
{
    public class NewGradeDto
    {
        public double Grade_value { get; set; }
        public DateTime Date { get; set; }
        public int Weight { get; set; }
        public string Description { get; set; }
        public int Id_subject { get; set; }
        public string Subject_name { get; set; }
    }
}
