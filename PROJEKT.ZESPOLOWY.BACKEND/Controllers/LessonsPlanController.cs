﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PROJEKT.ZESPOLOWY.BACKEND.DTOs;
using PROJEKT.ZESPOLOWY.BACKEND.Models;
using PROJEKT.ZESPOLOWY.BACKEND.Services;

namespace PROJEKT.ZESPOLOWY.BACKEND.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class LessonsPlanController : ControllerBase
    {
        private readonly ILessonsPlansService _lessonsPlanService;

        public LessonsPlanController(ILessonsPlansService lessonsPlanService)
        {
            _lessonsPlanService = lessonsPlanService;
        }

        [Route("GetLessonsPlanMonday")]
        [HttpGet]
        public IActionResult GetLessonsPlanMonday()
        {
            var result = _lessonsPlanService.GetLessonsPlanFromGivenDay("monday");
            return Ok(result);
        }

        [Route("GetLessonsPlanTuesday")]
        [HttpGet]
        public IActionResult GetLessonsPlanTuesday()
        {
            var result = _lessonsPlanService.GetLessonsPlanFromGivenDay("tuesday");
            return Ok(result);
        }

        [Route("GetLessonsPlanWednesday")]
        [HttpGet]
        public IActionResult GetLessonsPlanWednesday()
        {
            var result = _lessonsPlanService.GetLessonsPlanFromGivenDay("wednesday");
            return Ok(result);
        }

        [Route("GetLessonsPlanThursday")]
        [HttpGet]
        public IActionResult GetLessonsPlanThursday()
        {
            var result = _lessonsPlanService.GetLessonsPlanFromGivenDay("thursday");
            return Ok(result);
        }

        [Route("GetLessonsPlanFriday")]
        [HttpGet]
        public IActionResult GetLessonsPlanFriday()
        {
            var result = _lessonsPlanService.GetLessonsPlanFromGivenDay("friday");
            return Ok(result);
        }

        [Route("GetSingleLessonsPlan")]
        [HttpPost]
        public IActionResult GetSingleLessonsPlan([FromBody] int id)
        {
            var result = _lessonsPlanService.GetSingleLessonPlan(id);
            return Ok(result);
        }

        [Route("RemoveLessonsPlan")]
        [HttpPost]
        public IActionResult RemoveLessonsPlan([FromBody] int id)
        {
            _lessonsPlanService.RemoveLessonsPlan(id);
            return Ok();
        }

        [Route("AddLessonsPlan")]
        [HttpPost]
        public IActionResult AddLessonsPlan ([FromBody] NewLessonsPlanDto newLessonsPlan)
        {
            _lessonsPlanService.AddLessonsPlan(newLessonsPlan);
            return Ok();
        }

        [Route("UpdateLessonsPlan")]
        [HttpPost]
        public IActionResult UpdateLessonsPlan([FromBody] LessonsPlan lessonsPlan)
        {
            _lessonsPlanService.UpdateLessonsPlan(lessonsPlan);
            return Ok();
        }
    }
}
