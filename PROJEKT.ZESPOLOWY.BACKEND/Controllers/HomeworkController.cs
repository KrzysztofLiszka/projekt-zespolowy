﻿using ClosedXML.Excel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PROJEKT.ZESPOLOWY.BACKEND.DTOs;
using PROJEKT.ZESPOLOWY.BACKEND.Models;
using PROJEKT.ZESPOLOWY.BACKEND.Services;

namespace PROJEKT.ZESPOLOWY.BACKEND.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeworkController : ControllerBase
    {
        private readonly IHomeworksService _homeworksService;

        public HomeworkController(IHomeworksService homeworksService)
        {
            _homeworksService = homeworksService;
        }

        [Route("GetAllHomeworks")]
        [HttpGet]
        public IActionResult GetAllHomeworks()
        {
            var result = _homeworksService.GetAllHomeworks();
            return Ok(result);
        }

        [Route("GetSingleHomework")]
        [HttpPost]
        public IActionResult GetSingleHomework([FromBody] int id)
        {
            var result = _homeworksService.GetSingleHomework(id);
            return Ok(result);
        }

        [Route("RemoveHomework")]
        [HttpPost]
        public IActionResult RemoveHomework([FromBody] int id)
        {
            _homeworksService.RemoveHomework(id);
            return Ok();
        }

        [Route("AddHomework")]
        [HttpPost]
        public IActionResult AddHomework([FromBody] NewHomeworkDto newHomework)
        {
            _homeworksService.AddHomework(newHomework);
            return Ok();
        }

        [Route("UpdateHomework")]
        [HttpPost]
        public IActionResult UpdateHomework([FromBody] Homework homework)
        {
            _homeworksService.UpdateHomework(homework);
            return Ok();
        }  
    }
}
