﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PROJEKT.ZESPOLOWY.BACKEND.DTOs;
using PROJEKT.ZESPOLOWY.BACKEND.Models;
using PROJEKT.ZESPOLOWY.BACKEND.Services;

namespace PROJEKT.ZESPOLOWY.BACKEND.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubjectsController : ControllerBase
    {
        private readonly ISubjectsService _subjectsService;

        public SubjectsController(ISubjectsService subjectsService)
        {
            _subjectsService = subjectsService;
        }

        [Route("GetAllSubjects")]
        [HttpGet]
        public IActionResult GetAllSubjects()
        {
            var result = _subjectsService.GetAllSubjects();
            return Ok(result);
        }

        [Route("GetSingleSubject")]
        [HttpPost]
        public IActionResult GetSingleSubject([FromBody] int id)
        {
            var result = _subjectsService.GetSingleSubject(id);
            return Ok(result);
        }

        [Route("RemoveSubject")]
        [HttpPost]
        public IActionResult RemoveSubject([FromBody] int id)
        {
            _subjectsService.RemoveSubject(id);
            return Ok();
        }

        [Route("AddSubject")]
        [HttpPost]
        public IActionResult AddSubject([FromBody] NewSubjectDto newsubject)
        {
            _subjectsService.AddSubject(newsubject);
            return Ok();
        }

        [Route("UpdateSubject")]
        [HttpPost]
        public IActionResult UpdateSubject([FromBody] Subject subject)
        {
            _subjectsService.UpdateSubject(subject);
            return Ok();
        }
    }
}
