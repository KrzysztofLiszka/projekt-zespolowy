﻿using ClosedXML.Excel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PROJEKT.ZESPOLOWY.BACKEND.DTOs;
using PROJEKT.ZESPOLOWY.BACKEND.Models;
using PROJEKT.ZESPOLOWY.BACKEND.Services;

namespace PROJEKT.ZESPOLOWY.BACKEND.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class GradesController : ControllerBase
    {
        private readonly IGradesService _gradesService;
        
        public GradesController(IGradesService gradesService)
        {
            _gradesService = gradesService;
        }

        [Route("GetAllGrades/{subjectId}")]
        [HttpGet]
        public IActionResult GetAllGrades(int? subjectId)
        {
            var result = _gradesService.GetAllGrades(subjectId);
            return Ok(result);
        }
        
        [Route("GetSingleGrade")]
        [HttpPost]
        public IActionResult GetSingleGrade([FromBody] int id)
        {
            var result = _gradesService.GetSingleGrade(id);
            return Ok(result);
        }

        [Route("RemoveGrade")]
        [HttpPost]
        public IActionResult RemoveGrade([FromBody] int id)
        {
            _gradesService.RemoveGrade(id);
            return Ok();
        }

        [Route("AddGrade")]
        [HttpPost]
        public IActionResult AddGrade([FromBody] NewGradeDto newGrade)
        {
            _gradesService.AddGrade(newGrade);
            return Ok();
        }

        [Route("UpdateGrade")]
        [HttpPost]
        public IActionResult UpdateGrade([FromBody] Grade grade)
        {
            _gradesService.UpdateGrade(grade);
            return Ok();
        }

    }
}
