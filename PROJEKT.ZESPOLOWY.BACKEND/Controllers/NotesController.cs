using ClosedXML.Excel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PROJEKT.ZESPOLOWY.BACKEND.DTOs;
using PROJEKT.ZESPOLOWY.BACKEND.Models;
using PROJEKT.ZESPOLOWY.BACKEND.Services;

namespace PROJEKT.ZESPOLOWY.BACKEND.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class NotesController : ControllerBase
    {
        private readonly INotesService _notesService;

        public NotesController(INotesService notesService)
        {
            _notesService = notesService;
        }

        [Route("GetAllNotes")]
        [HttpGet]
        public IActionResult GetAllNotes()
        {
            var result = _notesService.GetAllNotes();
            return Ok(result);
        }

        [Route("GetSingleNote")]
        [HttpPost]
        public IActionResult GetSingleNote([FromBody] int id)
        {
            var result = _notesService.GetSingleNote(id);
            return Ok(result);
        }

        [Route("RemoveNote")]
        [HttpPost]
        public IActionResult RemoveNote([FromBody] int id)
        {
            _notesService.RemoveNote(id);
            return Ok();
        }

        [Route("AddNote")]
        [HttpPost]
        public IActionResult AddNote([FromBody] NewNoteDto newNote)
        {
            _notesService.AddNote(newNote);
            return Ok();
        }

        [Route("UpdateNote")]
        [HttpPost]
        public IActionResult UpdateNote([FromBody] Note note)
        {
            _notesService.UpdateNote(note);
            return Ok();
        }
    }
}

