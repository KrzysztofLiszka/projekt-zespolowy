﻿using ClosedXML.Excel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PROJEKT.ZESPOLOWY.BACKEND.DTOs;
using PROJEKT.ZESPOLOWY.BACKEND.Models;
using PROJEKT.ZESPOLOWY.BACKEND.Services;

namespace PROJEKT.ZESPOLOWY.BACKEND.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TeachersController : ControllerBase
    {
        private readonly ITeachersService _teachersService;

        public TeachersController(ITeachersService teachersService)
        {
             _teachersService = teachersService;
        }

        [Route("GetAllTeachers")]
        [HttpGet]
        public IActionResult GetAllTeachers()
        {
            var result = _teachersService.GetAllTeachers();
            return Ok(result);
        }

        [Route("GetSingleTeacher")]
        [HttpPost]
        public IActionResult GetSingleTeacher([FromBody] int id)
        {
            var result = _teachersService.GetSingleTeacher(id);
            return Ok(result);
        }

        [Route("RemoveTeacher")]
        [HttpPost]
        public IActionResult RemoveTeacher([FromBody] int id)
        {
            _teachersService.RemoveTeacher(id);
            return Ok();
        }

        [Route("AddTeacher")]
        [HttpPost]
        public IActionResult AddTeacher([FromBody] NewTeacherDto newTeacher)
        {
            _teachersService.AddTeacher(newTeacher);
            return Ok();
        }

        [Route("UpdateTeacher")]
        [HttpPost]
        public IActionResult UpdateTeacher([FromBody] Teacher teacher)
        {
            _teachersService.UpdateTeacher(teacher);
            return Ok();
        }


        [Route("GetAllTeachersXlsx")]
        [HttpGet]
        public IActionResult GetAllTeachersXlsx()
        {
            var teachers = _teachersService.GetAllTeachers();
            using var workbook = new XLWorkbook();
            var worksheet = workbook.Worksheets.Add("Teachers");

            // Set header row values and style
            var headerRow = worksheet.Row(1);
            headerRow.Cell(1).Value = "Imie";
            headerRow.Cell(2).Value = "Nazwisko";
            headerRow.Cell(3).Value = "Tytul naukowy";
            headerRow.Cell(4).Value = "Email";
            headerRow.Style.Font.Bold = true;

            // Set data row style
            var dataStyle = workbook.Style;
            var row = 2;
            foreach (var teacher in teachers)
            {
                worksheet.Cell(row, 1).Value = teacher.Name;
                worksheet.Cell(row, 2).Value = teacher.Surname;
                worksheet.Cell(row, 3).Value = teacher.AcademicTitle;
                worksheet.Cell(row, 4).Value = teacher.Email;
                worksheet.Row(row).Style = dataStyle;
                row++;
            }
            using var stream = new MemoryStream();
            workbook.SaveAs(stream);
            return new FileContentResult(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {
                FileDownloadName = "nauczyciele.xlsx"
            };
        }
    }
}

