﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PROJEKT.ZESPOLOWY.BACKEND.DTOs;
using PROJEKT.ZESPOLOWY.BACKEND.Models;
using PROJEKT.ZESPOLOWY.BACKEND.Services;

namespace PROJEKT.ZESPOLOWY.BACKEND.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SemestersController : ControllerBase
    {
        private readonly ISemestersService _semestersService;

        public SemestersController(ISemestersService semestersService)
        {
            _semestersService = semestersService;
        }

        [Route("GetAllSemesters")]
        [HttpGet]
        public IActionResult GetAllSemesters()
        {
            var result = _semestersService.GetAllSemesters();
            return Ok(result);
        }

        [Route("GetSingleSemester")]
        [HttpPost]
        public IActionResult GetSingleHomework([FromBody] int id)
        {
            var result = _semestersService.GetSingleSemester(id);
            return Ok(result);
        }

        [Route("RemoveSemester")]
        [HttpPost]
        public IActionResult RemoveSemester([FromBody] int id)
        {
            _semestersService.RemoveSemester(id);
            return Ok();
        }

        [Route("AddSemester")]
        [HttpPost]
        public IActionResult AddSemester([FromBody] NewSemesterDto newSemester)
        {
            _semestersService.AddSemester(newSemester);
            return Ok();
        }

        [Route("UpdateSemester")]
        [HttpPost]
        public IActionResult UpdateSemester([FromBody] Semester semester)
        {
            _semestersService.UpdateSemester(semester);
            return Ok();
        }
    }
}
