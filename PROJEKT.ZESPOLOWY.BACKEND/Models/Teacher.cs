﻿namespace PROJEKT.ZESPOLOWY.BACKEND.Models
{
    public class Teacher
    {
        public int Id { get; set; }
        public string? AcademicTitle { get; set; }
        public string? Name { get; set; }
        public string? Surname { get; set; }
        public string? Email { get; set; }

    }
}
