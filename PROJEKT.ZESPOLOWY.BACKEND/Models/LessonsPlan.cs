﻿namespace PROJEKT.ZESPOLOWY.BACKEND.Models
{
    public class LessonsPlan
    {
        public int Id { get; set; }
        public string StartHour { get; set; }
        public string EndHour { get; set; }
        public string Subject { get; set; }
        public string Day { get; set; }
    }
}
