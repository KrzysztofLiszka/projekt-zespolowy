namespace PROJEKT.ZESPOLOWY.BACKEND.Models
{
    public class Note
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Subject_name { get; set; }
        public DateTime Date { get; set; }
    }
}
