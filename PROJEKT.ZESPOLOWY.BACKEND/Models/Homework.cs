﻿namespace PROJEKT.ZESPOLOWY.BACKEND.Models
{
    public class Homework
    {
        public int Id { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string Subject { get; set; }
        public string Progress { get; set; } 
    }
}
