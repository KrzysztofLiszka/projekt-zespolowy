﻿using PROJEKT.ZESPOLOWY.BACKEND.DTOs;
using PROJEKT.ZESPOLOWY.BACKEND.Models;

namespace PROJEKT.ZESPOLOWY.BACKEND.Services
{
    public interface ISemestersService
    {
        void AddSemester(NewSemesterDto semester);
        List<Semester> GetAllSemesters();
        Semester GetSingleSemester(int id);
        void RemoveSemester(int id);
        void UpdateSemester(Semester semester);
    }
}