using PROJEKT.ZESPOLOWY.BACKEND.DTOs;
using PROJEKT.ZESPOLOWY.BACKEND.Models;

namespace PROJEKT.ZESPOLOWY.BACKEND.Services
{
    public interface INotesService
    {
        List<Note> GetAllNotes();
        Note GetSingleNote(int id);
        void AddNote(NewNoteDto note);
        void UpdateNote(Note note);
        void RemoveNote(int id);

    }
}
