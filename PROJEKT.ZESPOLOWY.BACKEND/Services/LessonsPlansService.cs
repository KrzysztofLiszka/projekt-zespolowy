﻿using PROJEKT.ZESPOLOWY.BACKEND.DTOs;
using PROJEKT.ZESPOLOWY.BACKEND.Models;
using PROJEKT.ZESPOLOWY.BACKEND.SqlRepository;

namespace PROJEKT.ZESPOLOWY.BACKEND.Services
{
    public class LessonsPlansService : ILessonsPlansService
    {
        private readonly ILogger<LessonsPlansService> _logger;
        private readonly ISqlRepository _sqlRepository;

        public LessonsPlansService(ILogger<LessonsPlansService> logger, ISqlRepository sqlRepository)
        {
            _logger = logger;
            _sqlRepository = sqlRepository;
        }

        public void AddLessonsPlan(NewLessonsPlanDto lessosPlan)
        {
            _logger.LogInformation("[START] Add new homework");
            _sqlRepository.AddUserItem(lessosPlan);
            _logger.LogInformation("[DONE] Add new homework");
        }

        public List<LessonsPlan> GetLessonsPlanFromGivenDay(string day)
        {
            _logger.LogInformation("[START] Get Lessons Plan From Given Day");
            var lessonsPlan = _sqlRepository.GetAllItemsFromUser<LessonsPlan>();
            var result = lessonsPlan.Where(plan => plan.Day == day).ToList();
            _logger.LogInformation("[DONE] Get Lessons Plan From Given Day");

            return result;
        }

        public LessonsPlan GetSingleLessonPlan(int id)
        {
            _logger.LogInformation("[START] Get Single Lesson Plan");
            var lessonsPlan = _sqlRepository.GetSingleItem<LessonsPlan>(id);
            _logger.LogInformation("[DONE] Get Single Lesson Plan");

            return lessonsPlan;
        }

        public void RemoveLessonsPlan(int id)
        {
            _logger.LogInformation("[START] Remove Lessons Plan");
            _sqlRepository.Remove<LessonsPlan>(id);
            _logger.LogInformation("[DONE] Remove Lessons Plan");
        }

        public void UpdateLessonsPlan(LessonsPlan lessonsPlan)
        {
            _logger.LogInformation("[START] Update Lessons Plan");
            _sqlRepository.Update(lessonsPlan);
            _logger.LogInformation("[DONE] Remove Lessons Plan");
        }
    }
}
