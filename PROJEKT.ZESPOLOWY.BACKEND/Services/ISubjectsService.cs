﻿using PROJEKT.ZESPOLOWY.BACKEND.DTOs;
using PROJEKT.ZESPOLOWY.BACKEND.Models;

namespace PROJEKT.ZESPOLOWY.BACKEND.Services
{
    public interface ISubjectsService
    {
        void AddSubject(NewSubjectDto subjects);
        List<Subject> GetAllSubjects();
        Subject GetSingleSubject(int id);
        void RemoveSubject(int id);
        void UpdateSubject(Subject subject);
    }
}