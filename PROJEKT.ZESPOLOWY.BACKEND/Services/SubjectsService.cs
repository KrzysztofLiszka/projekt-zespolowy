﻿using PROJEKT.ZESPOLOWY.BACKEND.DTOs;
using PROJEKT.ZESPOLOWY.BACKEND.Models;
using PROJEKT.ZESPOLOWY.BACKEND.SqlRepository;

namespace PROJEKT.ZESPOLOWY.BACKEND.Services
{
    public class SubjectsService : ISubjectsService
    {
        private readonly ILogger<SubjectsService> _logger;
        private readonly ISqlRepository _sqlRepository;

        public SubjectsService(ILogger<SubjectsService> logger, ISqlRepository sqlRepository)
        {
            _logger = logger;
            _sqlRepository = sqlRepository;
        }

        public void AddSubject(NewSubjectDto subjects)
        {
            _logger.LogInformation("[START] Add new subject");
            _sqlRepository.AddUserItem(subjects);
            _logger.LogInformation("[DONE] Add new subject");
        }

        public List<Subject> GetAllSubjects()
        {
            _logger.LogInformation("[START] Get all subjects");
            var subjects = _sqlRepository.GetAllItemsFromUser<Subject>();
            _logger.LogInformation("[DONE] Get all subjects");

            return subjects;
        }

        public Subject GetSingleSubject(int id)
        {
            _logger.LogInformation("[START] Get single subject");
            var subject = _sqlRepository.GetSingleItem<Subject>(id);
            _logger.LogInformation("[DONE] Get single subject");

            return subject;
        }

        public void RemoveSubject(int id)
        {
            _logger.LogInformation("[START] Delete subject");
            _sqlRepository.Remove<Subject>(id);
            _logger.LogInformation("[DONE] Delete subject");
        }

        public void UpdateSubject(Subject subject)
        {
            _logger.LogInformation("[START] Update subject");
            _sqlRepository.Update(subject);
            _logger.LogInformation("[DONE] Update subject");
        }
    }
}
