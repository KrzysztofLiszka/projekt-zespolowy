﻿using PROJEKT.ZESPOLOWY.BACKEND.DTOs;
using PROJEKT.ZESPOLOWY.BACKEND.Models;
using PROJEKT.ZESPOLOWY.BACKEND.SqlRepository;

namespace PROJEKT.ZESPOLOWY.BACKEND.Services
{
    public class TeachersService : ITeachersService
    {
        private readonly ILogger<TeachersService> _logger;
        private readonly ISqlRepository _sqlRepository;

        public TeachersService(ILogger<TeachersService> logger, ISqlRepository sqlRepository)
        {
            _logger = logger;
            _sqlRepository = sqlRepository;
        }

        public void AddTeacher(NewTeacherDto teacher)
        {
            _logger.LogInformation("[START] Add new teacher");
            _sqlRepository.AddUserItem(teacher);
            _logger.LogInformation("[DONE] Add new teacher");
        }

        public List<Teacher> GetAllTeachers()
        {
            _logger.LogInformation("[START] Get all teacher");
            var teachers = _sqlRepository.GetAllItemsFromUser<Teacher>();
            _logger.LogInformation("[DONE] Get all teacher");

            return teachers;
        }

        public Teacher GetSingleTeacher(int id)
        {
            _logger.LogInformation("[START] Get single teacher");
            var teacher = _sqlRepository.GetSingleItem<Teacher>(id);
            _logger.LogInformation("[DONE] Get single teacher");

            return teacher;
        }

        public void RemoveTeacher(int id)
        {
            _logger.LogInformation("[START] Delete teacher");
            _sqlRepository.Remove<Teacher>(id);
            _logger.LogInformation("[DONE] Delete teacher");
        }

        public void UpdateTeacher(Teacher teacher)
        {
            _logger.LogInformation("[START] Update teacher");
            _sqlRepository.Update(teacher);
            _logger.LogInformation("[DONE] Update teacher");
        }
    }
}
