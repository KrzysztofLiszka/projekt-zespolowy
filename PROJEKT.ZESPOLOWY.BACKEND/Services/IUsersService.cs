﻿using PROJEKT.ZESPOLOWY.BACKEND.DTOs;
using PROJEKT.ZESPOLOWY.BACKEND.Models;

namespace PROJEKT.ZESPOLOWY.BACKEND.Services
{
    public interface IUsersService
    {
        List<User> GetAllUsers();
        User GetUserById(int id);
        void AddUser(NewUserDto User);
        void RemoveUser(int id);
        void UpdateSemester(int userId, int semesterId);
    }
}
