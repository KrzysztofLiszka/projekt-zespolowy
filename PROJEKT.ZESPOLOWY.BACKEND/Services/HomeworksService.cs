﻿using PROJEKT.ZESPOLOWY.BACKEND.DTOs;
using PROJEKT.ZESPOLOWY.BACKEND.Models;
using PROJEKT.ZESPOLOWY.BACKEND.SqlRepository;

namespace PROJEKT.ZESPOLOWY.BACKEND.Services
{
    public class HomeworksService : IHomeworksService
    {
        
            private readonly ILogger<HomeworksService> _logger;
            private readonly ISqlRepository _sqlRepository;

            public HomeworksService(ILogger<HomeworksService> logger, ISqlRepository sqlRepository)
            {
                _logger = logger;
                _sqlRepository = sqlRepository;
            }

            public void AddHomework(NewHomeworkDto homework)
            {
                _logger.LogInformation("[START] Add new homework");
                homework.AddedDate = DateTime.Now;
                _sqlRepository.AddUserItem(homework);
                _logger.LogInformation("[DONE] Add new homework");
            }

            public List<Homework> GetAllHomeworks()
            {
                _logger.LogInformation("[START] Get all homeworks");
                var homeworks = _sqlRepository.GetAllItemsFromUser<Homework>();
                _logger.LogInformation("[DONE] Get all homeworks");

                return homeworks;
            }

            public Homework GetSingleHomework(int id)
            {
                _logger.LogInformation("[START] Get single homework");
                var homework = _sqlRepository.GetSingleItem<Homework>(id);
                _logger.LogInformation("[DONE] Get single homework");

                return homework;
            }

            public void RemoveHomework(int id)
            {
                _logger.LogInformation("[START] Delete homework");
                _sqlRepository.Remove<Homework>(id);
                _logger.LogInformation("[DONE] Delete homework");
            }

            public void UpdateHomework(Homework homework)
            {
                _logger.LogInformation("[START] Update homework");
                _sqlRepository.Update(homework);
                _logger.LogInformation("[DONE] Update homework");
            }
        
    }
}
