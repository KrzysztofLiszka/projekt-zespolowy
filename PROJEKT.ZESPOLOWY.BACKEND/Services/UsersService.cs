﻿using DocumentFormat.OpenXml.Spreadsheet;
using PROJEKT.ZESPOLOWY.BACKEND.DTOs;
using PROJEKT.ZESPOLOWY.BACKEND.Models;
using PROJEKT.ZESPOLOWY.BACKEND.SqlRepository;

namespace PROJEKT.ZESPOLOWY.BACKEND.Services
{
    public class UsersService : IUsersService
    {
        private readonly ILogger<UsersService> _logger;
        private readonly ISqlRepository _sqlRepository;

        public UsersService(ILogger<UsersService> logger, ISqlRepository sqlRepository)
        {
            _logger = logger;
            _sqlRepository = sqlRepository;
        }

        public void AddUser(NewUserDto user)
        {
            _logger.LogInformation("[START] Add new user");
            _sqlRepository.Add(user);
            _logger.LogInformation("[DONE] Add new user");
        }

        public List<User> GetAllUsers()
        {
            _logger.LogInformation("[START] Get all users");
            var users = _sqlRepository.GetAllItems<User>();
            _logger.LogInformation("[DONE] Get all users");

            return users;
        }

        public User GetUserById(int id)
        {
            _logger.LogInformation("[START] Get single user");
            var user = _sqlRepository.GetSingleItem<User>(id);
            _logger.LogInformation("[DONE] Get single user");

            return user;
        }

        public void RemoveUser(int id)
        {
            _logger.LogInformation("[START] Delete user");
            _sqlRepository.Remove<User>(id);
            _logger.LogInformation("[DONE] Delete user");
        }

        public void UpdateSemester(int userId, int semesterId)
        {
            _logger.LogInformation("[START] Update semester");
            var user = GetUserById(userId);
            user.SemesterId = semesterId;
            _sqlRepository.Update(user);
            _logger.LogInformation("[DONE] Update semester");
        }
    }
}
