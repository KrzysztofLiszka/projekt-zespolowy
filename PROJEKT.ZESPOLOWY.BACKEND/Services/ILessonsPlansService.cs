﻿using PROJEKT.ZESPOLOWY.BACKEND.DTOs;
using PROJEKT.ZESPOLOWY.BACKEND.Models;

namespace PROJEKT.ZESPOLOWY.BACKEND.Services
{
    public interface ILessonsPlansService
    {
        public void AddLessonsPlan(NewLessonsPlanDto lessosPlan);
        public List<LessonsPlan> GetLessonsPlanFromGivenDay(string day);
        public LessonsPlan GetSingleLessonPlan(int id);
        void UpdateLessonsPlan(LessonsPlan lessonsPlan);
        public void RemoveLessonsPlan(int id);
    }
}
