﻿using PROJEKT.ZESPOLOWY.BACKEND.DTOs;
using PROJEKT.ZESPOLOWY.BACKEND.Models;

namespace PROJEKT.ZESPOLOWY.BACKEND.Services
{
    public interface ITeachersService
    {
        List<Teacher> GetAllTeachers();
        Teacher GetSingleTeacher(int id);
        void AddTeacher(NewTeacherDto Teacher);
        void UpdateTeacher(Teacher Teacher);
        void RemoveTeacher(int id);
    }
}
