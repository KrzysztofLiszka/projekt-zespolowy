﻿using PROJEKT.ZESPOLOWY.BACKEND.DTOs;
using PROJEKT.ZESPOLOWY.BACKEND.Models;
using PROJEKT.ZESPOLOWY.BACKEND.SqlRepository;

namespace PROJEKT.ZESPOLOWY.BACKEND.Services
{
    public class GradesService : IGradesService
    {
        private readonly ILogger<GradesService> _logger;
        private readonly ISqlRepository _sqlRepository;

        public GradesService(ILogger<GradesService> logger, ISqlRepository sqlRepository)
        {
            _logger = logger;
            _sqlRepository = sqlRepository;
        }

        public void AddGrade(NewGradeDto grade)
        {
            _logger.LogInformation("[START] Add new grade");
            grade.Date = DateTime.Now;
            _sqlRepository.AddUserItem(grade);
            _logger.LogInformation("[DONE] Add new grade");
        }

        public List<Grade> GetAllGrades(int? subjectId)
        {
            _logger.LogInformation("[START] Get all grades");
            var grades = _sqlRepository.GetAllItemsFromUser<Grade>();
            if (subjectId != null && subjectId != -2)
            {
                grades = grades.Where(g => g.Id_subject == subjectId.Value).ToList();
            }
            _logger.LogInformation("[DONE] Get all grades");

            return grades;
        }


        public Grade GetSingleGrade(int id)
        {
            _logger.LogInformation("[START] Get single grade");
            var grade = _sqlRepository.GetSingleItem<Grade>(id);
            _logger.LogInformation("[DONE] Get single grade");

            return grade;
        }

        public void RemoveGrade(int id)
        {
            _logger.LogInformation("[START] Delete grade");
            _sqlRepository.Remove<Grade>(id);
            _logger.LogInformation("[DONE] Delete grade");
        }

        public void UpdateGrade(Grade grade)
        {
            _logger.LogInformation("[START] Update grade");
            _sqlRepository.Update(grade);
            _logger.LogInformation("[DONE] Update grade");
        }
    }
}
