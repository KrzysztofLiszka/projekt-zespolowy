﻿using PROJEKT.ZESPOLOWY.BACKEND.DTOs;
using PROJEKT.ZESPOLOWY.BACKEND.Models;
using PROJEKT.ZESPOLOWY.BACKEND.SqlRepository;

namespace PROJEKT.ZESPOLOWY.BACKEND.Services
{
    public interface IHomeworksService
    {
        public void AddHomework(NewHomeworkDto homework);
        public List<Homework> GetAllHomeworks();
        public Homework GetSingleHomework(int id);
        void UpdateHomework(Homework Homework);
        public void RemoveHomework(int id);
    }
}
