﻿namespace PROJEKT.ZESPOLOWY.BACKEND.Services
{
    public interface ICurrentUserService
    {
        int? GetCurrentUserId();
    }
}
