﻿using PROJEKT.ZESPOLOWY.BACKEND.DTOs;
using PROJEKT.ZESPOLOWY.BACKEND.Models;

namespace PROJEKT.ZESPOLOWY.BACKEND.Services
{
    public interface IGradesService
    {
        List<Grade> GetAllGrades(int? subjectId);
        Grade GetSingleGrade(int id);
        void AddGrade(NewGradeDto grade);
        void UpdateGrade(Grade grade);
        void RemoveGrade(int id);
    }
}
