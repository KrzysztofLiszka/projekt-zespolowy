﻿using PROJEKT.ZESPOLOWY.BACKEND.DTOs;
using PROJEKT.ZESPOLOWY.BACKEND.Models;
using PROJEKT.ZESPOLOWY.BACKEND.SqlRepository;

namespace PROJEKT.ZESPOLOWY.BACKEND.Services
{
    public class SemestersService : ISemestersService
    {
        private readonly ILogger<SemestersService> _logger;
        private readonly ISqlRepository _sqlRepository;

        public SemestersService(ILogger<SemestersService> logger, ISqlRepository sqlRepository)
        {
            _logger = logger;
            _sqlRepository = sqlRepository;
        }

        public void AddSemester(NewSemesterDto semester)
        {
            _logger.LogInformation("[START] Add new semester");
            _sqlRepository.AddUserItem(semester);
            _logger.LogInformation("[DONE] Add new semester");
        }

        public List<Semester> GetAllSemesters()
        {
            _logger.LogInformation("[START] Get all semesters");
            var semesters = _sqlRepository.GetAllItemsFromUser<Semester>();
            _logger.LogInformation("[DONE] Get all semesters");

            return semesters;
        }

        public Semester GetSingleSemester(int id)
        {
            _logger.LogInformation("[START] Get single semester");
            var semester = _sqlRepository.GetSingleItem<Semester>(id);
            _logger.LogInformation("[DONE] Get single semester");

            return semester;
        }

        public void RemoveSemester(int id)
        {
            _logger.LogInformation("[START] Delete semester");
            _sqlRepository.Remove<Semester>(id);
            _logger.LogInformation("[DONE] Delete semester");
        }

        public void UpdateSemester(Semester semester)
        {
            _logger.LogInformation("[START] Update semester");
            _sqlRepository.Update(semester);
            _logger.LogInformation("[DONE] Update semester");
        }
    }
}
