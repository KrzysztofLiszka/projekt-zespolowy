using PROJEKT.ZESPOLOWY.BACKEND.DTOs;
using PROJEKT.ZESPOLOWY.BACKEND.Models;
using PROJEKT.ZESPOLOWY.BACKEND.SqlRepository;

namespace PROJEKT.ZESPOLOWY.BACKEND.Services
{
    public class NotesService : INotesService
    {
        private readonly ILogger<NotesService> _logger;
        private readonly ISqlRepository _sqlRepository;

        public NotesService(ILogger<NotesService> logger, ISqlRepository sqlRepository)
        {
            _logger = logger;
            _sqlRepository = sqlRepository;
        }

        public void AddNote(NewNoteDto note)
        {
            _logger.LogInformation("[START] Add new note");
            note.Date = DateTime.Now;
            _sqlRepository.AddUserItem(note);
            _logger.LogInformation("[DONE] Add new note");
        }

        public List<Note> GetAllNotes()
        {
            _logger.LogInformation("[START] Get all notes");
            var notes = _sqlRepository.GetAllItemsFromUser<Note>();
            _logger.LogInformation("[DONE] Get all notes");

            return notes;
        }

        public Note GetSingleNote(int id)
        {
            _logger.LogInformation("[START] Get single note");
            var note = _sqlRepository.GetSingleItem<Note>(id);
            _logger.LogInformation("[DONE] Get single note");

            return note;
        }

        public void RemoveNote(int id)
        {
            _logger.LogInformation("[START] Delete note");
            _sqlRepository.Remove<Note>(id);
            _logger.LogInformation("[DONE] Delete note");
        }

        public void UpdateNote(Note note)
        {
            _logger.LogInformation("[START] Update note");
            _sqlRepository.Update(note);
            _logger.LogInformation("[DONE] Update note");
        }
    }
}