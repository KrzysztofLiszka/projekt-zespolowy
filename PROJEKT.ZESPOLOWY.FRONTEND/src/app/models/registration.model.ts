export interface Registration {
    id: number;
    username: string;
    password: string;
}