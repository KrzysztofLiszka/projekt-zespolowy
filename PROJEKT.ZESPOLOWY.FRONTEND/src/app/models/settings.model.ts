export interface Settings {
    id: string;
    name: string;
    startDate: string;
    endDate: string;
}