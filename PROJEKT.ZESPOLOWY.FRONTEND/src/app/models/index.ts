export * from './grade.model';
export * from './teacher.model';
export * from './note.model';
export * from './lessons-plan.model';
export * from './registration.model';