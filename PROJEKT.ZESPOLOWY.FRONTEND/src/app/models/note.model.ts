export interface Note {
    id: string;
    title: string;
    content: string;
    subject_name: string;
    date: Date;
}