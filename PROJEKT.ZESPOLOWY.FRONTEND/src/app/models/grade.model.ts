export interface Grade {
    id: string;
    grade_value: number;
    date: Date;
    weight: number;
    description: string;
    id_subject: number;
    id_user: number;
    subject_name: string;
}