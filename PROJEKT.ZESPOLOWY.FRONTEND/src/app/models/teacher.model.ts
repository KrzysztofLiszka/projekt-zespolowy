export interface Teacher {
    id: string;
    academicTitle: string;
    name: string;
    surname: string;
    email: string;
}