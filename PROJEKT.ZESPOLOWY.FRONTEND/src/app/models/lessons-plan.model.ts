export interface LessonsPlan {
    id: string;
    startHour: string;
    endHour: string;
    subject: string;
    day: string;
}