export interface Homework {
    id: string;
    addedDate: Date;
    deliveryDate: Date;
    subject: string;
    progress: string;
}