import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseHttpService } from '.';
import { Grade } from '../models';

@Injectable({
  providedIn: 'root'
})
export class GradesService extends BaseHttpService {

  getGrades(subjectId?: number): Observable<Grade[]> {
    if(subjectId === undefined) return this.getAll<Grade>('Grades/GetAllGrades/-2');
    else return this.getAll<Grade>('Grades/GetAllGrades/' + subjectId);
  }

  updateGrade(grade: Grade): Observable<Grade> {
    return this.post<Grade>('Grades/UpdateGrade', grade);
  }

  deleteGrade(grade: Grade): Observable<Grade> {
    return this.post<Grade>('Grades/RemoveGrade', grade.id)
  }

  addGrade(grade: any): Observable<any>{
    return this.post<any>('Grades/AddGrade', grade);
  }
}