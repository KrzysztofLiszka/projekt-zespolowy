import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { Observable } from 'rxjs';
import { Homework } from '../models/homework.model';

@Injectable({
  providedIn: 'root'
})
export class HomeworkService extends BaseHttpService{

  getHomeworks(): Observable<Homework[]> {
    return this.getAll<Homework>('Homework/GetAllHomeworks');
  }

  updateHomework(homework: Homework): Observable<Homework> {
    return this.post<Homework>('Homework/UpdateHomework', homework);
  }

  deleteHomework(homework: Homework): Observable<Homework> {
    return this.post<Homework>('Homework/RemoveHomework', homework.id)
  }

  addHomework(homework: Homework): Observable<any>{
    return this.post<any>('Homework/AddHomework', homework);
  }
}
