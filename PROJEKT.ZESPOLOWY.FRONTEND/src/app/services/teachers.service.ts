import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { Teacher } from '../models';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TeachersService extends BaseHttpService {
  getTeachers(): Observable<Teacher[]> {
    return this.getAll<Teacher>('Teachers/GetAllTeachers');
  }

  updateTeacher(teacher: Teacher): Observable<Teacher> {
    return this.post<Teacher>('Teachers/UpdateTeacher', teacher);
  }

  deleteTeacher(teacher: Teacher): Observable<Teacher> {
    return this.post<Teacher>('Teachers/RemoveTeacher', teacher.id)
  }

  addTeacher(teacher: any): Observable<any>{
    return this.post<any>('Teachers/AddTeacher', teacher);
  }
}
