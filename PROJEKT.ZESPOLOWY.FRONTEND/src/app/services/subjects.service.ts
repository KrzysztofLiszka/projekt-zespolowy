import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { Observable } from 'rxjs';
import { Subject } from '../models/subject.model';

@Injectable({
  providedIn: 'root'
})
export class SubjectsService extends BaseHttpService {

  getSubjects(): Observable<Subject[]> {
    return this.getAll<Subject>('Subjects/GetAllSubjects');
  }

  getSubjectById(id: number): Observable<any> {
    return this.post<any>('Subjects/GetSingleSubject', id);
  }

  updateSubject(subject: Subject): Observable<Subject> {
    return this.post<Subject>('Subjects/UpdateSubject', subject);
  }

  deleteSubject(subject: Subject): Observable<Subject> {
    return this.post<Subject>('Subjects/RemoveSubject', subject.id)
  }

  addSubject(subject: any): Observable<any> {
    return this.post<any>('Subjects/AddSubject', subject);
  }
}
