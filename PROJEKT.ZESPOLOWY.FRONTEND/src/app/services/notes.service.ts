import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { Note } from '../models';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotesService extends BaseHttpService {

  getNotes(): Observable<Note[]> {
    return this.getAll<Note>('Notes/GetAllNotes');
  }

  getNoteById(id: number): Observable<any> {
    return this.post<any>('Notes/GetSingleNote', id);
  }

  updateNote(note: Note): Observable<Note> {
    return this.post<Note>('Notes/UpdateNote', note);
  }

  deleteNote(note: Note): Observable<Note> {
    return this.post<Note>('Notes/RemoveNote', note.id)
  }

  addNote(note: any): Observable<any> {
    return this.post<any>('Notes/AddNote', note);
  }
}
