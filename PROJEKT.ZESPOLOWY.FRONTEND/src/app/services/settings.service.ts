import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { Settings } from '../models/settings.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SettingsService extends BaseHttpService {

  getSemestrs(): Observable<Settings[]> {
    return this.getAll<Settings>('Semesters/GetAllSemesters');
  }

  getSemesterById(id: number): Observable<any> {
    return this.post<any>('Semesters/GetSingleSemester', id);
  }

  updateSemester(semester: Settings): Observable<Settings> {
    return this.post<Settings>('Semesters/UpdateSemester', semester);
  }

  deleteSemester(semester: Settings): Observable<Settings> {
    return this.post<Settings>('Semesters/RemoveSemester', semester.id)
  }

  addSemester(semester: any): Observable<any> {
    return this.post<any>('Semesters/AddSemester', semester);
  }

  updateUserSemester(semesterId: number): Observable<any> {
    return this.post<any>('Auth/UpdateSemester', semesterId);
  }
}
