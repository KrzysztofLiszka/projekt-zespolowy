import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Registration } from '../models';
import { BaseHttpService } from './base-http.service';

@Injectable({
  providedIn: 'root',
})
export class RegistrationService extends BaseHttpService{

  registerUser(username: string, password: string): Observable<any> {
    const payload = { username, password };
    return this.post<Registration>('Auth/Register', payload);
  }
}
