import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { Observable } from 'rxjs';
import { LessonsPlan } from '../models';

@Injectable({
  providedIn: 'root'
})
export class LessonsPlanService extends BaseHttpService {

  getLessonsPlanMonday(): Observable<LessonsPlan[]> {
    return this.getAll<LessonsPlan>('LessonsPlan/GetLessonsPlanMonday');
  }

  getLessonsPlanTuesday(): Observable<LessonsPlan[]> {
    return this.getAll<LessonsPlan>('LessonsPlan/GetLessonsPlanTuesday');
  }

  getLessonsPlanWednesday(): Observable<LessonsPlan[]> {
    return this.getAll<LessonsPlan>('LessonsPlan/GetLessonsPlanWednesday');
  }

  getLessonsPlanThursday(): Observable<LessonsPlan[]> {
    return this.getAll<LessonsPlan>('LessonsPlan/GetLessonsPlanThursday');
  }

  getLessonsPlanFriday(): Observable<LessonsPlan[]> {
    return this.getAll<LessonsPlan>('LessonsPlan/GetLessonsPlanFriday');
  }

  updateLessonsPlan(lessonPlan: LessonsPlan): Observable<LessonsPlan> {
    return this.post<LessonsPlan>('LessonsPlan/UpdateLessonsPlan', lessonPlan);
  }

  deleteLessonsPlan(lessonPlan: LessonsPlan): Observable<LessonsPlan> {
    return this.post<LessonsPlan>('LessonsPlan/RemoveLessonsPlan', lessonPlan.id)
  }

  addLessonsPlan(lessonPlan: any): Observable<any> {
    return this.post<any>('LessonsPlan/AddLessonsPlan', lessonPlan);
  }
}