import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
    loggedUser: string;
    constructor(private authService: AuthService) { }
    ngOnInit(): void {
        this.setButtonClassFromUrl();
        this.loggedUser = sessionStorage.getItem('loggedUser')?.toUpperCase() ?? '';

    }

    currentDate: Date = new Date();
    dashboardClass: string = 'selected-button';
    gradesClass: string = 'not-selectedbutton';
    lessonsClass: string = 'not-selectedbutton';
    notesClass: string = 'not-selectedbutton';
    homeworksClass: string = 'not-selectedbutton';
    teachersClass: string = 'not-selectedbutton';
    settingsClass: string = 'not-selectedbutton';
    subjectsClass: string = 'not-selectedbutton';

    setSelectedButton(buttonName: string) {
        this.dashboardClass = buttonName === 'dashboard' ? 'selected-button' : 'not-selectedbutton';
        this.gradesClass = buttonName === 'grades' ? 'selected-button' : 'not-selectedbutton';
        this.lessonsClass = buttonName === 'lessons-plan' ? 'selected-button' : 'not-selectedbutton';
        this.notesClass = buttonName === 'notes' ? 'selected-button' : 'not-selectedbutton';
        this.homeworksClass = buttonName === 'homeworks' ? 'selected-button' : 'not-selectedbutton';
        this.teachersClass = buttonName === 'teachers' ? 'selected-button' : 'not-selectedbutton';
        this.settingsClass = buttonName === 'settings' ? 'selected-button' : 'not-selectedbutton';
        this.subjectsClass = buttonName === 'subjects' ? 'selected-button' : 'not-selectedbutton';
    }

    setButtonClassFromUrl(): void {
        const currentUrl = window.location.href;
        const urlParts = currentUrl.split('/');
        const pageName = urlParts[urlParts.length - 1];

        switch (pageName) {
            case 'dashboard':
                this.setSelectedButton('dashboard');
                break;
            case 'grades':
                this.setSelectedButton('grades');
                break;
            case 'lessons-plan':
                this.setSelectedButton('lessons-plan');
                break;
            case 'notes':
                this.setSelectedButton('notes');
                break;
            case 'homeworks':
                this.setSelectedButton('homeworks');
                break;
            case 'teachers':
                this.setSelectedButton('teachers');
                break;
            case 'settings':
                this.setSelectedButton('settings');
                break;
            case 'subjects':
                this.setSelectedButton('subjects');
                break;
            default:
                break;
        }
    }

    logout() {
        this.authService.logout();
    }

}
