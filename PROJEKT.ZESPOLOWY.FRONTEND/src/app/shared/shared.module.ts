import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent, TableComponent, TextEditorComponent } from './components';
import { AppRoutingModule } from '../app-routing.module';
import { MaterialModule } from '../material.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from '../interceptors';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const COMPONENTS = [MenuComponent, TableComponent, TextEditorComponent];

@NgModule({
  declarations: [COMPONENTS],
  imports: [
    CommonModule,
    AppRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    CKEditorModule,
    BrowserModule
  ],
  exports: [COMPONENTS],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  }]
})
export class SharedModule { }
