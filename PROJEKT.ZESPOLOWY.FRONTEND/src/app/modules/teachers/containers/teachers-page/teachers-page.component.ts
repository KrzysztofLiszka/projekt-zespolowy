import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Teacher } from 'src/app/models';
import { TeachersService } from 'src/app/services';
import { AddTeacherDialogComponent, EditTeacherDialogComponent } from '../../components';

@Component({
  selector: 'app-teachers-page',
  templateUrl: './teachers-page.component.html',
  styleUrls: ['./teachers-page.component.scss']
})
export class TeachersPageComponent implements OnInit, OnDestroy {
  private subscription = new Subscription;
  teachers: Teacher[] = [];
  displayedColumns: string[] = ['academicTitle', 'name', 'surname', 'email', 'actions'];
  displayedHeaders: string[] = ['Tytuł akademicki', 'Imie', 'Nazwisko', 'Email', 'Akcje'];
  
  constructor(private teachersService: TeachersService, private dialog: MatDialog, private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.fetchTeachers();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  addTeacher(): void{
    const dialogRef = this.dialog.open(AddTeacherDialogComponent);
    dialogRef.afterClosed().subscribe((result) => {
      if(!result) return;
      console.log(result);
      this.subscription.add(this.teachersService.addTeacher(result).subscribe());
      location.reload();
    });
  }

  editTeacher(teacher: Teacher): void{
    const dialogRef = this.dialog.open(EditTeacherDialogComponent, {
      data: { teacher: teacher },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if(!result) return;
      this.subscription.add(this.teachersService.updateTeacher(result).subscribe());
      location.reload();
    });
  }

  deleteTeacher(teacher: Teacher): void{
    this.subscription.add(this.teachersService.deleteTeacher(teacher).subscribe());
    location.reload();
  }

  private fetchTeachers(): void {
    this.subscription.add(this.teachersService.getTeachers().subscribe(res => {
      this.teachers = res as Teacher[];
    }));
  }

  downloadFile(): void {
    this.httpClient.get('https://localhost:7078/api/Teachers/GetAllTeachersXlsx', { responseType: 'blob' })
      .subscribe(response => {
        const url = window.URL.createObjectURL(new Blob([response]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', 'nauczyciele.xlsx');
        document.body.appendChild(link);
        link.click();
      });
  }
}
