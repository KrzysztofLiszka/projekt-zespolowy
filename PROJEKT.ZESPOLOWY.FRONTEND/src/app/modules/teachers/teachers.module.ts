import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeachersPageComponent } from './containers';
import { SharedModule } from 'src/app/shared/shared.module';
import { MaterialModule } from 'src/app/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddTeacherDialogComponent, EditTeacherDialogComponent } from './components';

const COMPONENTS = [TeachersPageComponent, AddTeacherDialogComponent, EditTeacherDialogComponent];

@NgModule({
  declarations: [COMPONENTS],
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [COMPONENTS]
})
export class TeachersModule { }
