import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-add-teacher-dialog',
  templateUrl: './add-teacher-dialog.component.html',
  styleUrls: ['./add-teacher-dialog.component.scss']
})
export class AddTeacherDialogComponent {
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddTeacherDialogComponent>
  ) {
    this.form = this.formBuilder.group({
      academicTitle: [null, Validators.required],
      name: ['', Validators.required],
      surname: [null, Validators.required],
      email: ['', Validators.required],
    });
  }

  onSubmit(): void {
    this.dialogRef.close(this.form.value);
  }
}