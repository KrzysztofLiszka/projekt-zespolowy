import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Teacher } from 'src/app/models';

@Component({
  selector: 'app-edit-teacher-dialog',
  templateUrl: './edit-teacher-dialog.component.html',
  styleUrls: ['./edit-teacher-dialog.component.scss']
})
export class EditTeacherDialogComponent {
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<EditTeacherDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { teacher: Teacher }
  ) {
    this.form = this.formBuilder.group({
      id: [data.teacher.id],
      academicTitle: [data.teacher.academicTitle, Validators.required],
      name: [data.teacher.name, Validators.required],
      surname: [data.teacher.surname, Validators.required],
      email: [data.teacher.email, Validators.required]
    });
  }

  onSubmit(): void {
    this.dialogRef.close(this.form.value);
  }
}