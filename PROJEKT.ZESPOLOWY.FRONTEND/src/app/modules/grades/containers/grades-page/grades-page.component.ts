import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Grade } from 'src/app/models/grade.model';
import { SubjectsService } from 'src/app/services';
import { Subject } from 'src/app/models/subject.model';

@Component({
    selector: 'app-grades-page',
    templateUrl: './grades-page.component.html',
    styleUrls: ['./grades-page.component.scss']
})
export class GradesPageComponent implements OnInit, OnDestroy {
    private subscription = new Subscription;
    grades: Grade[] = [];
    subjects: Subject[] = [];

    constructor(private subjectsService: SubjectsService) { }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
    ngOnInit(): void {
        this.subscription.add(this.subjectsService.getSubjects().subscribe(res => this.subjects = res as Subject[]))
    }

}
