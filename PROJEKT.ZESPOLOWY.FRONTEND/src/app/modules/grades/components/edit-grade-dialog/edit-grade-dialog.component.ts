import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Subscription } from 'rxjs/internal/Subscription';
import { Grade } from 'src/app/models/grade.model';
import { Subject } from 'src/app/models/subject.model';
import { SubjectsService } from 'src/app/services';

@Component({
  selector: 'app-edit-grade-dialog',
  templateUrl: './edit-grade-dialog.component.html',
  styleUrls: ['./edit-grade-dialog.component.scss']
})
export class EditGradeDialogComponent {
  form: FormGroup;
  subjects: Subject[] = [];
  private subscription = new Subscription;
  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<EditGradeDialogComponent>,
    private subjectsService:SubjectsService,
    @Inject(MAT_DIALOG_DATA) public data: { grade: Grade }
  ) {
    this.fetchSubjects();
    this.form = this.formBuilder.group({
      id: [data.grade.id],
      grade_value: [data.grade.grade_value, Validators.required],
      date: [data.grade.date, Validators.required],
      weight: [data.grade.weight, Validators.required],
      subject_name: [data.grade.subject_name, Validators.required],
      description: [data.grade.description, Validators.required],
      id_subject: [data.grade.id_subject],
    });
  }

  onSubmit(): void {
    this.dialogRef.close(this.form.value);
  }

  private fetchSubjects(): void {
    this.subscription.add(this.subjectsService.getSubjects().subscribe(res => {
      this.subjects = res as Subject[];
    }));
  }
}
