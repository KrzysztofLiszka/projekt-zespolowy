import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Grade } from 'src/app/models';
import { EditGradeDialogComponent } from '../edit-grade-dialog/edit-grade-dialog.component';
import { AddGradeDialogComponent } from '../add-grade-dialog/add-grade-dialog.component';
import { GradesService, SubjectsService } from 'src/app/services';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Subject } from 'src/app/models/subject.model';

@Component({
  selector: 'app-grades-table',
  templateUrl: './grades-table.component.html',
  styleUrls: ['./grades-table.component.scss']
})
export class GradesTableComponent implements OnInit, OnDestroy {
    @Input() subjectId: number = -2;
    private subscription = new Subscription;
    grades: Grade[] = [];
    subjects: Subject[] = [];
    displayedColumns: string[] = ['grade_value', 'date', 'weight', 'description', 'actions'];
    displayedHeaders: string[] = ['Ocena', 'Data', 'Waga oceny', 'Opis', 'Akcje'];

    constructor(private gradesService: GradesService, private dialog: MatDialog, private subjectsService: SubjectsService) { }

    ngOnInit(): void {
        this.fetchItems();
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    addGrade(): void {
        const dialogRef = this.dialog.open(AddGradeDialogComponent);
        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            this.subjectsService.getSubjectById(result.id_subject).subscribe((subject) => {
                result.subject_name = subject.name;
                this.subscription.add(this.gradesService.addGrade(result).subscribe());
                location.reload();
            });
        });
    }

    editGrade(grade: Grade): void {
        const dialogRef = this.dialog.open(EditGradeDialogComponent, {
            data: { grade: grade },
        });
        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            this.subjectsService.getSubjectById(result.id_subject).subscribe((subject) => {
                result.subject_name = subject.name;
                this.subscription.add(this.gradesService.updateGrade(result).subscribe());
                location.reload();
            });
        });
    }

    deleteGrade(grade: Grade): void {
        this.subscription.add(this.gradesService.deleteGrade(grade).subscribe());
        location.reload();
    }

    private fetchItems(): void {
        this.subscription.add(this.gradesService.getGrades(this.subjectId).subscribe(res => {
            this.grades = res as Grade[];
        }));
        this.subscription.add(this.subjectsService.getSubjects().subscribe(res => {
            this.subjects = res as Subject[];
        }));
    }
}
