import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { EditGradeDialogComponent } from '../edit-grade-dialog/edit-grade-dialog.component';
import { SubjectsService } from 'src/app/services';
import { Subject } from 'src/app/models/subject.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-add-grade-dialog',
  templateUrl: './add-grade-dialog.component.html',
  styleUrls: ['./add-grade-dialog.component.scss']
})
export class AddGradeDialogComponent {
  form: FormGroup;
  subjects: Subject[] = [];
  private subscription = new Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddGradeDialogComponent>,
    private subjectsService:SubjectsService
  ) {
    this.fetchSubjects();
    this.form = this.formBuilder.group({
      grade_value: [null, Validators.required],
      date: [Date.now, Validators.required],
      weight: [null, Validators.required],
      subject_name: ['', Validators.required],
      description: [null, Validators.required],
      id_subject: [0],
      id_user: [1]
    });
  }

  onSubmit(): void {
    this.dialogRef.close(this.form.value);
  }

  private fetchSubjects(): void {
    this.subscription.add(this.subjectsService.getSubjects().subscribe(res => {
      this.subjects = res as Subject[];
    }));
  }
}
