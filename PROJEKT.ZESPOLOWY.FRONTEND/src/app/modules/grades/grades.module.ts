import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GradesPageComponent } from './containers';
import { MaterialModule } from 'src/app/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AddGradeDialogComponent, EditGradeDialogComponent, GradesTableComponent } from './components';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


const COMPONENTS = [GradesPageComponent, EditGradeDialogComponent, AddGradeDialogComponent, GradesTableComponent];

@NgModule({
  declarations: [COMPONENTS],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [COMPONENTS]
})
export class GradesModule { }
