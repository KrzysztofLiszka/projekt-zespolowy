import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-add-note-dialog',
  templateUrl: './add-note-dialog.component.html',
  styleUrls: ['./add-note-dialog.component.scss']
})
export class AddNoteDialogComponent {
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddNoteDialogComponent>
  ) {
    this.form = this.formBuilder.group({
      title: [null, Validators.required],
      date: [Date.now],
      content: [""],
      subject_name: ['', Validators.required]
    });
  }

  onSubmit(): void {
    this.dialogRef.close(this.form.value);
  }
}