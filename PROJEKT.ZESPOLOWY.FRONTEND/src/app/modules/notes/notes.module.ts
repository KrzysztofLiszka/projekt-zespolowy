import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditNotePageComponent, NotesPageComponent } from './containers';
import { SharedModule } from 'src/app/shared/shared.module';
import { AddNoteDialogComponent } from './components';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material.module';

const COMPONENTS = [NotesPageComponent, AddNoteDialogComponent, EditNotePageComponent];

@NgModule({
  declarations: [COMPONENTS],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  exports: [COMPONENTS]
})
export class NotesModule { }
