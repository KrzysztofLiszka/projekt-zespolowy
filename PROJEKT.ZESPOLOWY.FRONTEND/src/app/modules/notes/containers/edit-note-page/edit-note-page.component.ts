import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Note } from 'src/app/models';
import { NotesService } from 'src/app/services';

@Component({
  selector: 'app-edit-note-page',
  templateUrl: './edit-note-page.component.html',
  styleUrls: ['./edit-note-page.component.scss']
})
export class EditNotePageComponent implements OnInit, OnDestroy {
  form: FormGroup;
  entityId: string = '';
  subscription = new Subscription;

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private notesService: NotesService,private router: Router) {
    this.form = this.formBuilder.group({
      id: ['', Validators.required],
      title: ['', Validators.required],
      date: [Date.now, Validators.required],
      content: ['', Validators.required],
      subject_name: ['', Validators.required]
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngOnInit(): void {
    var idInString = this.route.snapshot.paramMap.get('id');
    if (!idInString) return;
    var id: number = +idInString;
    this.notesService.getNoteById(id).subscribe(response => this.form.patchValue(response));
  }

  save(): void {
    this.subscription.add(this.notesService.updateNote(this.form.value as Note).subscribe(() => {
      this.router.navigate(['/notes']); // Przekierowanie do '/notes'
    }));
  }
}
