import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Note } from 'src/app/models';
import { NotesService } from 'src/app/services';
import { AddNoteDialogComponent } from '../../components';
import { Router } from '@angular/router';

@Component({
  selector: 'app-notes-page',
  templateUrl: './notes-page.component.html',
  styleUrls: ['./notes-page.component.scss']
})
export class NotesPageComponent {
  private subscription = new Subscription;
  notes: Note[] = [];
  displayedColumns: string[] = ['title', 'date', 'subject_name', 'actions'];
  displayedHeaders: string[] = ['Tytuł', 'Data dodania', 'Przedmiot', 'Akcje'];

  ngOnInit(): void {
    this.fetchNotes();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  constructor(private notesService: NotesService, private dialog: MatDialog, private router: Router) { }

  addNote(): void {
    const dialogRef = this.dialog.open(AddNoteDialogComponent);
    dialogRef.afterClosed().subscribe((result) => {
      if (!result) return;
      console.log(result);
      this.subscription.add(this.notesService.addNote(result).subscribe());
      location.reload();
    });
  }

  editNote(note: any): void {
    this.router.navigate(['/notes', note.id]);
  }

  deleteNote(note: any): void {
    this.subscription.add(this.notesService.deleteNote(note).subscribe());
    location.reload();
  }

  private fetchNotes(): void {
    this.subscription.add(this.notesService.getNotes().subscribe(response => this.notes = response));
  }
}
