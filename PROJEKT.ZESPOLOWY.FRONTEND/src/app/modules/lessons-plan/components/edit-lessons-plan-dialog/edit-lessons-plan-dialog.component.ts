import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { LessonsPlan } from 'src/app/models';

@Component({
  selector: 'app-edit-lessons-plan-dialog',
  templateUrl: './edit-lessons-plan-dialog.component.html',
  styleUrls: ['./edit-lessons-plan-dialog.component.scss']
})
export class EditLessonsPlanDialogComponent {
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<EditLessonsPlanDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { lessonPlan: LessonsPlan }
  ) {
    this.form = this.formBuilder.group({
      id: [data.lessonPlan.id],
      startHour: [data.lessonPlan.startHour, Validators.required],
      endHour: [data.lessonPlan.endHour, Validators.required],
      subject: [data.lessonPlan.subject, Validators.required],
      day: [data.lessonPlan.day, Validators.required]
    });
  }

  onSubmit(): void {
    this.dialogRef.close(this.form.value);
  }
}
