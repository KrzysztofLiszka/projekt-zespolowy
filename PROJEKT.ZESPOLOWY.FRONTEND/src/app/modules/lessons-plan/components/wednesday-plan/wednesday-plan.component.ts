import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { LessonsPlan } from 'src/app/models';
import { LessonsPlanService } from 'src/app/services';
import { MatDialog } from '@angular/material/dialog';
import { AddLessonsPlanDialogComponent, EditLessonsPlanDialogComponent } from '..';

@Component({
  selector: 'app-wednesday-plan',
  templateUrl: './wednesday-plan.component.html',
  styleUrls: ['./wednesday-plan.component.scss']
})
export class WednesdayPlanComponent implements OnInit, OnDestroy {
  items: LessonsPlan[] = [];
  displayedColumns: string[] = ['startHour', 'endHour', 'subject', 'actions'];
  displayedHeaders: string[] = ['Godzina rozpoczęcia', 'Godzina zakończenia', 'Przedmiot', 'Akcje'];
  private subscription = new Subscription;

  constructor(private planService: LessonsPlanService, private dialog: MatDialog) { }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngOnInit(): void {
    this.fetchItems();
  }

  private fetchItems(): void {
    this.subscription.add(this.planService.getLessonsPlanWednesday().subscribe(res => this.items = res));
  }

  addLessonsPlan(): void{
    const dialogRef = this.dialog.open(AddLessonsPlanDialogComponent);
    dialogRef.afterClosed().subscribe((result) => {
      if(!result) return;
      this.subscription.add(this.planService.addLessonsPlan(result).subscribe());
      location.reload();
    });
  }

  editLessonsPlan(lessonPlan: LessonsPlan): void {
    const dialogRef = this.dialog.open(EditLessonsPlanDialogComponent, {
      data: { lessonPlan: lessonPlan },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (!result) return;
      this.subscription.add(this.planService.updateLessonsPlan(result).subscribe());
      location.reload();
    });
  }

  deleteLessonsPlan(lessonPlan: LessonsPlan): void {
    this.subscription.add(this.planService.deleteLessonsPlan(lessonPlan).subscribe());
    location.reload();
  }
}
