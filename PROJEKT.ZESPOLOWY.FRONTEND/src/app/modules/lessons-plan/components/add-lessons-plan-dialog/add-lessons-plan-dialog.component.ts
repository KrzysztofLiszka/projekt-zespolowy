import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-add-lessons-plan-dialog',
  templateUrl: './add-lessons-plan-dialog.component.html',
  styleUrls: ['./add-lessons-plan-dialog.component.scss']
})
export class AddLessonsPlanDialogComponent {
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddLessonsPlanDialogComponent>
  ) {
    this.form = this.formBuilder.group({
      startHour: [null, Validators.required],
      endHour: [null, Validators.required],
      subject: [null, Validators.required],
      day: [null, Validators.required]
    });
  }

  onSubmit(): void {
    this.dialogRef.close(this.form.value);
  }
}
