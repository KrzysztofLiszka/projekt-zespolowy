import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LessonsPlanComponent } from './containers';
import { MaterialModule } from 'src/app/material.module';
import {
  AddLessonsPlanDialogComponent, EditLessonsPlanDialogComponent, FridayPlanComponent, MondayPlanComponent, ThursdayPlanComponent, TuesdayPlanComponent, WednesdayPlanComponent
} from './components';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';


const COMPONENTS = [
  LessonsPlanComponent, MondayPlanComponent,
  TuesdayPlanComponent, WednesdayPlanComponent,
  ThursdayPlanComponent, FridayPlanComponent,
  AddLessonsPlanDialogComponent, EditLessonsPlanDialogComponent];

@NgModule({
  declarations: [COMPONENTS],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    ReactiveFormsModule
  ],
  exports: [COMPONENTS]
})
export class LessonsPlanModule { }
