import { Component, OnDestroy, OnInit } from '@angular/core';
import { SubjectsService } from 'src/app/services/subjects.service';
import { Subscription } from 'rxjs';
import { Grade } from 'src/app/models/grade.model';
import { MatDialog } from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import { AddSubjectComponent } from '../../components/add-subject/add-subject.component';
import { EditSubjectComponent } from '../../components/edit-subject/edit-subject.component';
import { Subject } from 'src/app/models/subject.model';

@Component({
  selector: 'app-subjects-page',
  templateUrl: './subjects-page.component.html',
  styleUrls: ['./subjects-page.component.scss']
})
export class SubjectsPageComponent implements OnInit, OnDestroy{

  
    private subscription = new Subscription;
    subjects: Subject[] = [];
    displayedColumns: string[] = ['name','actions'];
    displayedHeaders: string[] = ['Nazwa', 'Akcje'];
  
    constructor(private subjectService: SubjectsService, private dialog: MatDialog, private httpClient: HttpClient) { }
  
    ngOnInit(): void {
      this.fetchSubjects();
    }
  
    ngOnDestroy(): void {
      this.subscription.unsubscribe();
    }
  
    addSubject(): void{
      const dialogRef = this.dialog.open(AddSubjectComponent);
      dialogRef.afterClosed().subscribe((result) => {
        if(!result) return;
        this.subscription.add(this.subjectService.addSubject(result).subscribe());
        location.reload();
      });
    }
  
    editSubject(subject: Subject): void{
      const dialogRef = this.dialog.open(EditSubjectComponent, {
        data: { subject: subject },
      });
      dialogRef.afterClosed().subscribe((result) => {
        if(!result) return;
        this.subscription.add(this.subjectService.updateSubject(result).subscribe());
        location.reload();
      });
    }
  
    deleteSubject(subject: Subject): void{
      this.subscription.add(this.subjectService.deleteSubject(subject).subscribe());
      location.reload();
    }
  
    private fetchSubjects(): void {
      this.subscription.add(this.subjectService.getSubjects().subscribe(res => {
        this.subjects = res as Subject[];
      }));
    }
    
  }
