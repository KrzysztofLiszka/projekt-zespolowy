import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'src/app/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddSubjectComponent } from './components/add-subject/add-subject.component';
import { EditSubjectComponent } from './components/edit-subject/edit-subject.component';
import { SubjectsPageComponent } from './containers/subjects-page/subjects-page.component';



const COMPONENTS = [AddSubjectComponent,EditSubjectComponent];

@NgModule({
  declarations: [COMPONENTS, SubjectsPageComponent],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [COMPONENTS],
})
export class SubjectsModule { }
