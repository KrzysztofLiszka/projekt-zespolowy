import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Subject } from 'src/app/models/subject.model';

@Component({
  selector: 'app-edit-subject',
  templateUrl: './edit-subject.component.html',
  styleUrls: ['./edit-subject.component.scss']
})
export class EditSubjectComponent {
form: FormGroup;

constructor(
  private formBuilder: FormBuilder,
  private dialogRef: MatDialogRef<EditSubjectComponent>,
  @Inject(MAT_DIALOG_DATA) public data: { subject: Subject }
) {
  this.form = this.formBuilder.group({
    id: [data.subject.id],
    grade_value: [data.subject.name, Validators.required],

  });
}

onSubmit(): void {
  this.dialogRef.close(this.form.value);
}
}
