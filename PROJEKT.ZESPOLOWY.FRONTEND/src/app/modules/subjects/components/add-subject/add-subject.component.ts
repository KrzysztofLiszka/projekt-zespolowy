import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-add-subject',
  templateUrl: './add-subject.component.html',
  styleUrls: ['./add-subject.component.scss']
})
export class AddSubjectComponent {
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddSubjectComponent>
  ) {
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      id_user: [1]
    });
  }

  onSubmit(): void {
    this.dialogRef.close(this.form.value);
  }
}
