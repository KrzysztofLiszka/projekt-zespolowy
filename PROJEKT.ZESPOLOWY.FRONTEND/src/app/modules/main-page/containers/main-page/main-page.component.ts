import { Component, OnInit } from '@angular/core';
import { LessonsPlanService } from "../../../../services";
import { LessonsPlan } from "../../../../models";
import { GradesService } from "../../../../services/grades.service";
import { Grade } from "../../../../models";

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {
  daysToFinish: number | undefined;
  classEndDate: Date = new Date('2023-06-30');
  schedule: LessonsPlan[] = [];
  gradesLast30Days: Grade[] = [];
  grades: Grade[] = [];
  averageGrade: number = 0;


  constructor(
    private lessonsPlanService: LessonsPlanService,
    private gradesService: GradesService
  ) {
  }

  ngOnInit() {
    this.getLessonsPlanForCurrentDay();
    this.calculateDaysToEndOfClasses();
    this.getGradesLast30Days();
    this.calculateGradesAverage();
  }

  getLessonsPlanForCurrentDay() {
    const currentDay = new Date().getDay();

    switch (currentDay) {
      case 1:
        this.lessonsPlanService.getLessonsPlanMonday().subscribe(
          (response: LessonsPlan[]) => {
            this.schedule = response;
          },
          (error) => {
            console.error(error);
          }
        );
        break;
      case 2:
        this.lessonsPlanService.getLessonsPlanTuesday().subscribe(
          (response: LessonsPlan[]) => {
            this.schedule = response;
          },
          (error) => {
            console.error(error);
          }
        );
        break;
      case 3:
        this.lessonsPlanService.getLessonsPlanWednesday().subscribe(
          (response: LessonsPlan[]) => {
            this.schedule = response;
          },
          (error) => {
            console.error(error);
          }
        );
        break;
      case 4:
        this.lessonsPlanService.getLessonsPlanThursday().subscribe(
          (response: LessonsPlan[]) => {
            this.schedule = response;
          },
          (error) => {
            console.error(error);
          }
        );
        break;
      case 5:
        this.lessonsPlanService.getLessonsPlanFriday().subscribe(
          (response: LessonsPlan[]) => {
            this.schedule = response;
          },
          (error) => {
            console.error(error);
          }
        );
        break;
      default:
        break;
    }
  }

  calculateDaysToEndOfClasses() {
    const currentDate = new Date();
    const endDate = new Date(this.classEndDate);
    const timeDiff = endDate.getTime() - currentDate.getTime();
    const daysDiff = Math.ceil(timeDiff / (1000 * 3600 * 24));
    this.daysToFinish = daysDiff;
  }

  formatEndDate() {
    return this.classEndDate.toLocaleDateString('pl-PL', {day: '2-digit', month: 'long', year: 'numeric'});
  }

  getGradesLast30Days() {
    const currentDate = new Date();
    const thirtyDaysAgo = new Date();
    thirtyDaysAgo.setDate(thirtyDaysAgo.getDate() - 30);

    this.gradesService.getGrades().subscribe((grades: Grade[]) => {
      this.gradesLast30Days = grades.filter((grade: Grade) =>
        new Date(grade.date) >= thirtyDaysAgo && new Date(grade.date) <= currentDate
      );
    });
  }

  calculateGradesAverage() {
    this.gradesService.getGrades().subscribe((grades: Grade[]) => {
      this.grades = grades;

      let sum = 0;
      for (const grade of this.grades) {
        sum += grade.grade_value;
      }
      const average = sum / this.grades.length;
      this.averageGrade = isNaN(average) ? 0 : parseFloat(average.toFixed(2));
    });
  }
}
