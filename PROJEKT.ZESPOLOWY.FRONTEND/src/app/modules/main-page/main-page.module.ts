import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MainPageComponent} from './containers';
import {MatIconModule} from "@angular/material/icon";
import {MatCardModule} from "@angular/material/card";
import {AuthService} from "../../services/auth.service";
import {AuthGuard} from "../../services/auth-guard.service";

const COMPONENTS = [MainPageComponent];

@NgModule({
  declarations: [COMPONENTS],
  imports: [
    CommonModule,
    MatIconModule,
    MatCardModule,
  ],
  providers: [AuthService, AuthGuard],
  exports: [COMPONENTS]
})
export class MainPageModule { }
