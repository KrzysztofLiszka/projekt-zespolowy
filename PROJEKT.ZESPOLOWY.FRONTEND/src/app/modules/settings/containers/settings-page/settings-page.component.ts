import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { Settings } from 'src/app/models/settings.model';
import { AddSemesterComponent } from '../../components/add-semester/add-semester.component';
import { SettingsService } from 'src/app/services';
import { MatDialog } from '@angular/material/dialog';
import { EditSemesterComponent } from '../../components/edit-semester/edit-semester.component';

@Component({
    selector: 'app-settings-page',
    templateUrl: './settings-page.component.html',
    styleUrls: ['./settings-page.component.scss']
})
export class SettingsPageComponent implements OnInit, OnDestroy {
    @ViewChild('fileInput') fileInput: ElementRef;
    fileAttr = 'Wybierz';
    selectedFile: File | undefined;
    selectedImageBase64: string | undefined;
    private subscription = new Subscription;
    semesters: Settings[] = [];
    displayedColumns: string[] = ['name', 'startDate', 'endDate', 'actions', 'select'];
    displayedHeaders: string[] = ['Nazwa', 'Data startu', 'Data zakończenia', 'Akcje', 'Wybierz semestr'];

    constructor(private settingsService: SettingsService, private dialog: MatDialog) { }

    ngOnInit(): void {
        this.fetchSemesters();
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    uploadFileEvt(imgFile: any) {
        if (imgFile.target.files && imgFile.target.files[0]) {
            this.selectedFile = imgFile.target.files[0];
            this.fileAttr = '';
            Array.from(imgFile.target.files).forEach((file: any) => {
                this.fileAttr += file.name + ' - ';
            });
            let reader = new FileReader();
            reader.onload = (e: any) => {
                this.selectedImageBase64 = e.target.result;
            };
            reader.readAsDataURL(imgFile.target.files[0]);
            this.fileInput.nativeElement.value = '';
        } else {
            this.selectedFile = undefined;
            this.fileAttr = 'Wybierz';
        }
    }

    addSemester(): void {
        const dialogRef = this.dialog.open(AddSemesterComponent);
        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            console.log(result);
            this.subscription.add(this.settingsService.addSemester(result).subscribe());
            location.reload();
        });
    }

    editSemester(semester: Settings): void {
        const dialogRef = this.dialog.open(EditSemesterComponent, {
            data: { settings: semester },
        });
        dialogRef.afterClosed().subscribe((result) => {
            if (!result) return;
            this.subscription.add(this.settingsService.updateSemester(result).subscribe());
            location.reload();
        });
    }

    deleteSemester(semester: Settings): void {
        this.subscription.add(this.settingsService.deleteSemester(semester).subscribe());
        location.reload();
    }

    formatDate(date: string): string {
        const options: Intl.DateTimeFormatOptions = { day: 'numeric', month: 'long', year: 'numeric' };
        const formattedDate = new Date(date).toLocaleDateString('pl-PL', options);
        return formattedDate.replace(/(\d+)(?:st|nd|rd|th)/, '$1');
    }


    private fetchSemesters(): void {
        this.subscription.add(this.settingsService.getSemestrs().subscribe((res: Settings[]) => {
            this.semesters = res.map((semester: Settings) => {
                return {
                    ...semester,
                    startDate: this.formatDate(semester.startDate),
                    endDate: this.formatDate(semester.endDate)
                };
            });
        }));
    }

    updateUserSemester(semester: any): void {
        var semesterId = semester.id;
        localStorage.setItem('semesterId', semesterId);
        this.subscription.add(this.settingsService.updateUserSemester(semesterId).subscribe());
    }

}
