import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Settings } from 'src/app/models/settings.model';


@Component({
  selector: 'app-edit-semester',
  templateUrl: './edit-semester.component.html',
  styleUrls: ['./edit-semester.component.scss']
})
export class EditSemesterComponent {
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<EditSemesterComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { settings: Settings }
  ) {
    this.form = this.formBuilder.group({
      id: [data.settings.id],
      name: [data.settings.name, Validators.required],
      startDate: [data.settings.startDate, Validators.required],
      endDate: [data.settings.endDate, Validators.required],
    });
  }

  onSubmit(): void {
    this.dialogRef.close(this.form.value);
  }
}
