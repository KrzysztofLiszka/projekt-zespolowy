import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsPageComponent } from './containers/settings-page/settings-page.component';
import { MaterialModule } from 'src/app/material.module';
import { AddSemesterComponent } from './components/add-semester/add-semester.component';
import { EditSemesterComponent } from './components/edit-semester/edit-semester.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [SettingsPageComponent, AddSemesterComponent, EditSemesterComponent],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [SettingsPageComponent],
})
export class SettingsModule {}
