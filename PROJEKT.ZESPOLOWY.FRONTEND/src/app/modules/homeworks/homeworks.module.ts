import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeworksPageComponent } from './containers';
import { EditHomeworkComponent } from './components/edit-homework/edit-homework.component';
import { AddHomeworkComponent } from './components/add-homework/add-homework.component';
import { MaterialModule } from 'src/app/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const COMPONENTS = [HomeworksPageComponent,AddHomeworkComponent,EditHomeworkComponent];

@NgModule({
  declarations: [COMPONENTS],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [COMPONENTS],
})
export class HomeworksModule {}
