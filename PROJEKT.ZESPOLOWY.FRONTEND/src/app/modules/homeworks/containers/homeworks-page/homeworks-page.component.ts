import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Homework } from 'src/app/models/homework.model';
import { HomeworkService } from 'src/app/services/homework.service';
import { AddHomeworkComponent } from '../../components/add-homework/add-homework.component';
import { EditHomeworkComponent } from '../../components/edit-homework/edit-homework.component';

@Component({
  selector: 'app-homeworks-page',
  templateUrl: './homeworks-page.component.html',
  styleUrls: ['./homeworks-page.component.scss'],
})
export class HomeworksPageComponent implements OnInit, OnDestroy{
  private subscription = new Subscription;
  homeworks: Homework[] = [];
  displayedColumns: string[] = ['addedDate', 'deliveryDate', 'subject', 'progress', 'actions'];
  displayedHeaders: string[] = ['Data dodania', 'Data oddania', 'Przedmiot', 'Progress','Akcje'];

  constructor(private homeworkService: HomeworkService, private dialog: MatDialog, private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.fetchHomeworks();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  addHomework(): void{
    const dialogRef = this.dialog.open(AddHomeworkComponent);
    dialogRef.afterClosed().subscribe((result) => {
      if(!result) return;
      console.log(result);
      this.subscription.add(this.homeworkService.addHomework(result).subscribe());
      location.reload();
    });
  }

  editHomework(homework: Homework): void{
    const dialogRef = this.dialog.open(EditHomeworkComponent, {
      data: { homework: homework },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if(!result) return;
      this.subscription.add(this.homeworkService.updateHomework(result).subscribe());
      location.reload();
    });
  }

  deleteHomework(homework: Homework): void{
    this.subscription.add(this.homeworkService.deleteHomework(homework).subscribe());
    location.reload();
  }

  private fetchHomeworks(): void {
    this.subscription.add(this.homeworkService.getHomeworks().subscribe(res => {
      this.homeworks = res as Homework[];
    }));
  }

}
