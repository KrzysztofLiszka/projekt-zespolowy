import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Homework } from 'src/app/models/homework.model';

@Component({
  selector: 'app-edit-homework',
  templateUrl: './edit-homework.component.html',
  styleUrls: ['./edit-homework.component.scss']
})
export class EditHomeworkComponent {
  form: FormGroup;
  progressOptions: string[] = ['Do zrobienia', 'Gotowe do oceny', 'Ocenione'];

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<EditHomeworkComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { homework: Homework }
  ) {
    this.form = this.formBuilder.group({
      id: [data.homework.id],
      addedDate: [data.homework.addedDate, Validators.required],
      deliveryDate: [data.homework.deliveryDate, Validators.required],
      subject: [data.homework.subject, Validators.required],
      progress: [data.homework.progress, Validators.required],
    });
  }

  onSubmit(): void {
    this.dialogRef.close(this.form.value);
  }
}
