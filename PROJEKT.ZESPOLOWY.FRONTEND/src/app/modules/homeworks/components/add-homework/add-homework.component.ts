import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-add-homework',
  templateUrl: './add-homework.component.html',
  styleUrls: ['./add-homework.component.scss']
})
export class AddHomeworkComponent {
  form: FormGroup;
  progressOptions: string[] = ['Do zrobienia', 'Gotowe do oceny', 'Ocenione'];
  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddHomeworkComponent>
  ) {
    this.form = this.formBuilder.group({
      addedDate: [Date.now, Validators.required],
      deliveryDate: ['', Validators.required],
      subject: ['', Validators.required],
      progress: ['Do zrobienia', Validators.required],
      id_user: [1] 
    });
  }

  onSubmit(): void {
    this.dialogRef.close(this.form.value);
  }

}
