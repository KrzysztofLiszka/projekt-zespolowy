import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { MainPageModule } from './modules/main-page/main-page.module';
import { GradesModule } from './modules/grades/grades.module';
import { TeachersModule } from './modules/teachers/teachers.module';
import { NotesModule } from './modules/notes/notes.module';
import { LessonsPlanModule } from './modules/lessons-plan/lessons-plan.module';
import { HomeworksModule } from './modules/homeworks/homeworks.module';
import { LoginModule } from './modules/login/login.module';
import { HeaderComponent } from './shared/components';
import { MaterialModule } from './material.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterModule } from './modules/register/register.module';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './services/auth-guard.service';
import { GradesService, LessonsPlanService } from "./services";
import { SubjectsModule } from './modules/subjects/subjects.module';
import { FormsModule } from '@angular/forms';
import { SettingsModule } from './modules/settings/settings.module';

@NgModule({
  declarations: [AppComponent, HeaderComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    MainPageModule,
    RegisterModule,
    GradesModule,
    TeachersModule,
    NotesModule,
    MainPageModule,
    LessonsPlanModule,
    HomeworksModule,
    LoginModule,
    MaterialModule,
    HttpClientModule,
    SubjectsModule,
    FormsModule,
    SettingsModule,
  ],
  providers: [AuthService, AuthGuard, GradesService, LessonsPlanService],
  bootstrap: [AppComponent],
})
export class AppModule {}
